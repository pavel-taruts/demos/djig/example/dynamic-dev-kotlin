package org.taruts.djig.example.dynamicImpl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.taruts.djig.example.dynamicApi.dynamic.GreetingProvider

@Component
class GreetingProviderImpl : GreetingProvider {

    @Value("\${app.greeting}")
    private val greeting: String? = null

    override fun getGreeting(): String {
        return greeting!!
    }
}
