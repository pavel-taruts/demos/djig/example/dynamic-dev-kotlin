package org.taruts.djig.example.dynamicImpl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.taruts.djig.example.dynamicApi.dynamic.PunctuationMarkProvider

@Component
class PunctuationMarkProviderImpl : PunctuationMarkProvider {

    @Value("\${app.punctuation-mark}")
    private val punctuationMark: String? = null

    override fun getPunctuationMark(): String {
        return punctuationMark!!
    }
}
