package org.taruts.djig.example.dynamicImpl

import org.apache.commons.numbers.complex.Complex
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.taruts.djig.example.dynamicApi.dynamic.GreetingProvider
import org.taruts.djig.example.dynamicApi.dynamic.MessageProvider
import org.taruts.djig.example.dynamicApi.dynamic.NameProvider
import org.taruts.djig.example.dynamicApi.dynamic.PunctuationMarkProvider
import org.taruts.djig.example.dynamicApi.main.MessagePrefixProvider

@Component
class MessageProviderImpl : MessageProvider {

    @Autowired
    private lateinit var messagePrefixProvider: MessagePrefixProvider

    @Autowired
    private lateinit var greetingProvider: GreetingProvider

    @Autowired
    private lateinit var nameProvider: NameProvider

    @Autowired
    private lateinit var punctuationMarkProvider: PunctuationMarkProvider

    override fun getMessage(): String {

        // Using the commons-numbers-complex dependency that the dynamic project has but the main app does not
        val negativeValue: Complex = Complex.ofCartesian(-4.0, 0.0)
        val sqrtResult: Complex = negativeValue.sqrt()

        return messagePrefixProvider.messagePrefix + " " +
                greetingProvider.greeting + ", " +
                nameProvider.name +
                punctuationMarkProvider.punctuationMark + " " +
                "Did you know, that the square root of -4 is " + sqrtResult + "?"
    }
}
