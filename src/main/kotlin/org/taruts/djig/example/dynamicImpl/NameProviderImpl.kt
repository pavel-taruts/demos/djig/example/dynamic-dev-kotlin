package org.taruts.djig.example.dynamicImpl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.taruts.djig.example.dynamicApi.dynamic.NameProvider

@Component
class NameProviderImpl : NameProvider {

    @Value("\${app.name}")
    private val name: String? = null

    override fun getName(): String {
        return name!!
    }
}
